#!/bin/bash
echo " +-------------------------------------------------------+"$'\n' \
  $'| NetHunter bootanimation config script 1.0 by yesimxev |'$'\n' \
  $'^=======================================================^'$'\n'
while true; do
read -p "Please select from the following styles [1]"$'\n'$'1) Default'$'\n'$'2) Burning NetHunter'$'\n'$'3) New Kali Boot'$'\n'$'4) Kali-Glitch'$'\n' type
type=${type:-1}
   case $type in
        [1]*) srcdir="src" && break;;
        [2]*) srcdir="src_mk" && break;;
        [3]*) srcdir="src_kali" && break;;
		[4]*) srcdir="src_glitch" && break;;
        *) echo "Invalid input" >&2
   esac
done
size=$(dumpsys window | grep mOverscanScreen | awk '{print $2}')
echo $'\n'"[i] Your screen size is "$size$'\n'
read -p "Please enter the target resolution [1080x1920]"$'\n' res
res=${res:-1080x1920}
read -p "Please enter the target fps [60]"$'\n' fps
fps=${fps:-60}
while true; do
read -p "Do you need the images to be converted [y/N]? Try n first!"$'\n' req
req=${req:-n}
   case $req in
	[yY]*) convert=y && command -v convert >/dev/null 2>&1 || { echo >&2 "Converting requires imagemagick, please install it!"; exit 1;} && break;;
	[nN]*) convert=n && break;;
	*) echo "Invalid input" >&2
   esac
done
if [ "$convert" == "y" ]; then
   mkdir -p new/part0
   mkdir -p new/part1
if [ "$type" == "1" ] || [ "$type" == "3" ]; then mkdir -p new/part2; fi
echo $'\n'"[i] Converting images.."
   for i in {0000..0100}; do
	convert -resize $res $srcdir/part0/$i.jpg new/part0/$i.jpg >/dev/null 2>&1;
   done
echo "[+] part0 done"
   for i in {0000..0200}; do
	convert -resize $res $srcdir/part1/$i.jpg new/part1/$i.jpg >/dev/null 2>&1;
   done
echo "[+] part1 done"
if [ "$type" == "1" ] || [ "$type" == "3" ]; then
   for i in {0000..0300}; do
	convert -resize $res $srcdir/part2/$i.jpg new/part2/$i.jpg >/dev/null 2>&1;
   done
echo "[+] part2 done";
fi
echo "[+] Convert done"
else
	echo "[i] Adding source parts.."
	mkdir new
	cp -r $srcdir/part* new/
	echo "[+] parts copied"
fi
cp $srcdir/desc.txt new/
sed -i "1s/.*/$res $fps/" new/desc.txt
sed -i 's/x/ /g' new/desc.txt
echo "[+] Setting resolution and fps in desc "
cd new
   zip -0 -FSr -q ../output/bootanimation.zip *
cd ..
echo "[+] bootanimation.zip succesfully created"$'\n'
rm -r new
echo "Please install with ./install.sh, otherwise you can use ./buildtwrp.sh to make a TWRP flashable zip"$'\n'
echo "And remember, respect is everything!"
